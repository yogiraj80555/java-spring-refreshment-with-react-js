package com.springRest.Spring.Project.Services;

import java.util.List;

import com.springRest.Spring.Project.Entity.Course;

public interface CourseService {
	
	public List<Course> getCourses();
	public Course getCourse(String id);
	public Course addCourse(Course c);
	public Course updateCourse(Course c);
	public void deleteCourse(String id);

}
