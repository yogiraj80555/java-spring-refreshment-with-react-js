package com.springRest.Spring.Project.Services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.springRest.Spring.Project.DaoWork.CourseDao;
import com.springRest.Spring.Project.Entity.Course;




@Service
public class CourseServiceImpl implements CourseService {
	//List<Course> list;
	
	@Autowired
	private CourseDao dao;
	
	public CourseServiceImpl() {
		//list = new ArrayList<>();
		//list.add(new Course(1,"CSE", "Computer Science"));
		//list.add(new Course(2,"EnTC", "Electronics"));
		
		
	}
	
	
	
	@Override
	public List<Course> getCourses() {
		//return list;
		
		return dao.findAll();//pick data from database and add to list and return list
	}







	@Override
	public Course getCourse(String ids) {
		long id;
		try {
			id = Long.parseLong(ids);
			}catch(NumberFormatException e) {
				return new Course(000,"Enter Valid ID", "Please Enter valid Course ID");
			}
		/*
		for(Course c:list) {
			if(c.getId() == id) {
				return c;
			}
		}
		return new Course(00,"Not Found","No Such Id avalible");
		*/
		List<Course> l = dao.findAll();
		for( Course co : l) {
			if(co.getId() == id) {
				return co;
			}
		}
		
		return  new Course(000,"Not Found","Not Found");
	}



	@Override
	public Course addCourse(Course c) {
		//this.list.add(c);
		dao.save(c);
		return c;
	}



	@Override
	public Course updateCourse(Course course) {
		/*for(Course c : list) {
			if(c.getId() == course.getId()) {
				c.setTitle(course.getTitle());
				c.setDescription(course.getDescription());
				return c;
			}
		}
		return new Course(000,"Not Found", "Data Not Found to Updated");
		
		*/
		dao.save(course);
		//if course is already available then it update else it add new one
		
		return course;
	}
	
	
	
	@Override
	public void deleteCourse(String cid)throws NumberFormatException {
		long id;
		
		id = Long.parseLong(cid);
		
		/*
		for(int i=0; i< list.size(); i++) {
			
			if(id == list.get(i).getId()) {
				Course c = list.get(i);
				list.remove(i);
				return c;
			}
		}
		 return new Course(000,"Not Found", "Data Not Found for Deletion");
		*/
		 Course record = dao.getOne(id);
		 dao.delete(record);
		
	}

}
