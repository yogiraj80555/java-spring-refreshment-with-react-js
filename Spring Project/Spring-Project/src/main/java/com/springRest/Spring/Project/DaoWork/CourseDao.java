package com.springRest.Spring.Project.DaoWork;

import org.springframework.data.jpa.repository.JpaRepository;

import com.springRest.Spring.Project.Entity.Course;


//here extends "Course" as a SQL table content and 'Long' as primary key for table 
public interface CourseDao extends JpaRepository<Course, Long> {

}
