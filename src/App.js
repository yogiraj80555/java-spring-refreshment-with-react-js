
import './App.css';
import {Button,Container,Col,Row} from 'reactstrap';
import {ToastContainer, toast} from "react-toastify";
import AllCourses from "./Components/AllCourses";
import AddCourse from './Components/AddCourse';
import Header from './Components/Header';
import Menus from './Components/Menus';
import Home from './Components/Home';

import { BrowserRouter as Router, Route} from "react-router-dom";


function App() {

  const toastMessage = () => {
    toast("This is Toast Message",{
      position: "bottom-center"
    });
  };


  return (
    <div>
      <Router>
        <ToastContainer />
        <Container >
          <Header/>
          <Row>
            <Col md={4} >
              <Menus />
            </Col>
            <Col md={8}>
           
              <Route path="/" component={Home} exact />
              <Route path="/add-course" component={AddCourse} exact />
              <Route path="/allcourses" component={AllCourses} exact />
              
              <Route path="/update" component={AddCourse} exact />

            </Col>
          </Row>
        </Container>
      </Router>
    </div>
    
  );
}

export default App;
