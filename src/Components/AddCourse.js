import axios from "axios";
import React, {Fragment, useEffect,useState} from "react";
import { toast } from "react-toastify";
import { Form, FormGroup, Input } from "reactstrap";
import {
    Button,
    Container,
} from "reactstrap";
import base_url from "../API/Apis";


const AddCourse = () =>{
    useEffect(() => {
        document.title= "Adding/Updating Course";
    },[]) // [] <- so it run only single time 


const createCourse =(data) =>{
    console.log(course);
    data.preventDefault();
    data.target.reset();
    postService(course);
}


const postService=(data)=>{
    axios.post(`${base_url}/courses`,data).then(
        (response)=>{
            //console.log(response);
            response.status === 200 ?  toast.success("Course Added Successfully",{
                position: 'bottom-right',
            }) 
            :  toast.error("Somthing went Wrong",{
                position: 'bottom-right',
            });
            setCourse({ id: "", title: "", description: "" });
        },
        (error)=>{
            toast.warning("Somthing went Wrong",{
                position: 'bottom-right',
            });
        }
    )
}




const [course, setCourse] =  useState({});




return <Fragment>
    <h3  className="text-center text-weight-bold mt-3">Fill Course Details</h3>
    <Form onSubmit={createCourse}>
        <FormGroup className="mt-2">
            <label for="courseid"> Course ID </label>
            <Input type="number" placeholder="Enter Course ID" required name="courseid" id="courseid "
                onChange={(e) => {
                    setCourse({ ...course, id: e.target.value });
                }}
            />
        </FormGroup>

        <FormGroup className="mt-2">
            <label for="coursetitle"> Course Title </label>
            <Input type="text" placeholder="Enter Course Title" required name="coursetitle" id="coursetitle"
                onChange={(e) => {
                    setCourse({...course, title: e.target.value});
                }}
            />
        </FormGroup>

        <FormGroup className="mt-2">
            <label for="coursedescription"> Course Description </label>
            <Input type="textarea" 
                    placeholder="Enter Course Description" 
                    name="coursedescription" 
                    id="coursedescription"
                    required
                    style={{ height: 100, width:400}}
                    onChange={(e)=>{  setCourse({...course, description: e.target.value}); 
                    }}
                />
        </FormGroup>

        <Container className="text-center ">
            <Button className="m-2" type="submit" outline color="success">Add Course</Button>
            <Button className="m-2"  color="warning" 
                onClick={()=>{
                    setCourse({
                        id: "", title: "", description: ""
                    })
                }}
                >Clear</Button>
        </Container>
    </Form>

</Fragment>

}


export default AddCourse; 