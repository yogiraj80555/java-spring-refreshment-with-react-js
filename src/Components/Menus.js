import React from 'react';
import {ListGroup} from 'reactstrap';
import {Link} from "react-router-dom";

const Menus = ( ) => {
    
    return(
        <ListGroup>
            <Link className="list-group-item list-group-item-action " tag="a" to="/" action="true"> Home </Link>
            <Link className="list-group-item list-group-item-action " tag="a" to="/allcourses" action="true"> View All Courses </Link>
            <Link className="list-group-item list-group-item-action " tag="a" to="/add-course" action="true"> Add/Update New Course </Link>
            
            <a className="list-group-item list-group-item-action" target="_blank" href="https://yogiraj80555.github.io/YogirajPortfolio/">About Me!</a>
            
    
        </ListGroup>

    );

};


export default Menus;