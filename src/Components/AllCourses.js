import React, { useState, useEffect } from 'react'
import Course from "./Course"
import base_url from "../API/Apis";
import axios from 'axios';
import { toast } from 'react-toastify';

const AllCourse=()=>{


   


    useEffect(() => {
        document.title= "All Courses";
        getAllFromServer();
    },[]) // [] <- so it run only single time 

    const [courses,setCourses] = useState(
        [
        ]
    );



const updateCourseList=(id)=>{  //update course list when deleted
    setCourses(courses.filter((c) => c.id != id ));
}



    const getAllFromServer =  () => {
        axios.get(`${base_url}/courses`).then(
            (response)=>{
                
               setCourses(response.data)
            },
            (error)=>{
                console.log(error);
                toast.warning("Somthing went wrong.",{
                    position:'bottom-right',
                });
            }
            
        );
    }

 



    return(

        <div>
            <h1>All Courses</h1>
            <p>List of Courses are avalible</p>
            {
               courses.length > 0 
               ? courses.map ((item) => <Course key={item.id}  course={item} update={updateCourseList} />  )
               : "No Course"
            }
        </div>
    );



};


export default AllCourse;